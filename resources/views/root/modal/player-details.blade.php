<div class="modal-body p-0">
    <div class="modal-header bg-dark">
        <h5 class="modal-title mb-4">@if(isset($blade->title)) {{ucfirst($blade->title)}} @endif Histories </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="text-white">×</span>
        </button>
    </div>
    <div class="card mr-3 ml-3" style="margin-top:-3%">
        <div class="card-body p-3">
            <div class="row">
                <div class="col-md-2">
                    <img src="{{ asset('uploads/players/'.Players()) }}" class="img-circle elevation-2" style="width:60px;height:60px">
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3"><strong>Name</strong></div>
                        <div class="col-md-9">{{$blade->basic->first_name}} {{$blade->basic->last_name}}</div>
                        <div class="col-md-3"><strong>Specialist</strong></div>
                        <div class="col-md-9">{{ucwords(str_replace('_',' ',$blade->basic->specialist))}}</div>
                        <div class="col-md-3"><strong>Team</strong></div>
                        <div class="col-md-4">{{$blade->basic->teams->name}}</div>
                    </div>
                </div>
                <div class="col-md-2">
                    <img src="{{ asset('uploads/logo/'.$blade->basic->teams->file) }}" class="img-circle elevation-2" style="width:60px;height:60px">
                </div>
                <div class="col-md-2">
                    <img src="{{ asset('uploads/specialist/'.$blade->basic->file) }}" class="img-circle elevation-2" style="width:60px;height:60px;float: right;">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="example1" class="table table-bordered table-striped mt-3 p-0">
                        <thead>
                            <tr>
                                <th width="5%" class="text-center">Country</th>  
                                <th width="35%">Series</th>           
                                <th width="5%" class="text-center">Matches</th>           
                                <th width="5%" class="text-center">Wickets</th>           
                                <th width="5%" class="text-center">Runs</th>           
                                <th width="7%" class="text-center">Higest Score</th>           
                                <th width="5%" class="text-center">50's</th>           
                                <th width="5%" class="text-center">100's</th>           
                                <th width="5%" class="text-center">6's</th>           
                                <th width="5%" class="text-center">4's</th>           
                                <th width="5%" class="text-center">Duck</th>        
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($blade->history) && count($blade->history)>0 && $blade->history !=null)
                                @foreach ($blade->history as $row)
                                    <tr>
                                        <td class="text-center">@includeIf('root.comman.image',['dir'=>'logo','file'=>$row->series->teams->file])</td>
                                        <td>{{$row->series->title}}</td>
                                        <td class="text-center">{{$row->matches}}</td>
                                        <td class="text-center">{{$row->wickets}}</td>
                                        <td class="text-center">{{$row->runs}}</td>
                                        <td class="text-center">{{$row->highest_score}}</td>
                                        <td class="text-center">{{$row->fifties}}</td>
                                        <td class="text-center">{{$row->hundreds}}</td>
                                        <td class="text-center">{{$row->sixes}}</td>
                                        <td class="text-center">{{$row->fours}}</td>
                                        <td class="text-center">{{$row->duck}}</td>                              
                                    </tr>
                                @endforeach
                            @else
                            <tr><td colspan="3"><h5 style="text-align:center">No Record found!</h5></td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>                           
</div>