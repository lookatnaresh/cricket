@extends('layouts.index')
@section('main-content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-dark">
                        <div class="card-body p-0 table-responsive mailbox-messages">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="10%">Team</th>           
                                        <th width="5%" class="text-center">Won</th>           
                                        <th width="5%" class="text-center">Lost</th>           
                                        <th width="5%" class="text-center">Tied</th>           
                                        <th width="5%" class="text-center">No Result</th>           
                                        <th width="5%" class="text-center">Points</th>           
                                        <th width="5%">Action</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($blade->data) && count($blade->data)>0 && $blade->data !=null)
                                        @foreach ($blade->data as $row)
                                            <tr>
                                                <td>@includeIf('root.comman.image',['dir'=>'logo','file'=>$row->teams->file,'name'=>$row->teams->name])</td>          
                                                <td class="text-center text-success">{{$row->won}}</td>
                                                <td class="text-center text-danger">{{$row->lost}}</td>
                                                <td class="text-center text-warning">{{$row->tied}}</td>
                                                <td class="text-center text-info">{{$row->noresult}}</td>
                                                <td class="text-center">{{$row->points}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="cursor:pointer;float:right">
                                                            <i class="fas fa-ellipsis-v"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" role="menu" style="">
                                                            <a href="{{url('teams/players/'.$row->id)}}" class="text-primary view-file dropdown-item"  title="Click here to view players" ><i class="fa fa-search"></i> Players</a>
                                                        </div>
                                                    </div>
                                                </td>                                                                           
                                            </tr>
                                        @endforeach
                                    @else
                                    <tr><td colspan="3"><h5 style="text-align:center">No Record found!</h5></td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer text-center"> 
                            {{ $blade->data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection