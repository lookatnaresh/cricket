@php $path = public_path('uploads/'.$dir.'/'.$file); @endphp
@if(file_exists($path))
<div>
    <img src="{{ asset('uploads/'.$dir.'/'.$file) }}" class="img-circle elevation-2" style="width:20px;height:20px">
    @if(isset($name))
        &nbsp;{{$name}}
    @endif
</div>
@endif