              
<li class="nav-item d-none d-sm-inline-block">
    <a href="{{url('admin-dashboard')}}" class="nav-link">Cricket</a>
</li>
@if(isset($blade->breadcrumb))
    @foreach($blade->breadcrumb as $key => $value)
        @if($key === 'active')
            <li class="nav-item d-none d-sm-inline-block active">
                <a class="nav-link"> <i class="fas fa-angle-right" aria-hidden="true"></i> {{$value}}</a>
            </li>
        @else
        <li class="nav-item d-none d-sm-inline-block">
            <a class="nav-link"> <i class="fa fa-angle-right" aria-hidden="true"></i> {{$value}}</a>
        </li>
        @endif
    @endforeach
@endif
@if(isset($blade->base) && $blade->base !=null)
    @push('BScript')
        <script> 
            var $current_url = "{{ url($blade->base) }}" ;
        </script>
    @endpush
@endif

@push('BScript')
    <script> 
        var $base_url = "{{ url('/') }}" ;
    </script>
@endpush
