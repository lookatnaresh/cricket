
<div class="btn-group">
    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="cursor:pointer;float:right">
        <i class="fas fa-ellipsis-v"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-right" role="menu" style="">
        <a href="javascript:void(0)" data-id="{{$row->id}}" class="text-primary view-file dropdown-item"  title="Click here to view" ><i class="fa fa-search"></i> View</a>
    </div>
</div>