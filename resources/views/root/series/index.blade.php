@extends('layouts.index')
@section('main-content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-dark">
                        <div class="card-body p-0 table-responsive mailbox-messages">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="30%">Title</th>           
                                        <th width="10%">Hosting Countries</th>           
                                        <th width="5%" class="text-center">Tour Year</th>           
                                        <th width="5%" class="text-center">Series</th>           
                                        <th width="5%">Action</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($blade->data) && count($blade->data)>0 && $blade->data !=null)
                                        @foreach ($blade->data as $row)
                                            <tr>
                                                <td>{{ucfirst($row->title)}}</td>
                                                <td>@includeIf('root.comman.image',['dir'=>'logo','file'=>$row->teams->file,'name'=>$row->teams->name])</td>
                                                <td class="text-center">{{$row->tour_year}}</td>
                                                <td class="text-center">{{ucfirst($row->type)}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="cursor:pointer;float:right">
                                                            <i class="fas fa-ellipsis-v"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" role="menu" style="">
                                                            <a href="{{url('series/matches/'.$row->id.'/fixtures')}}" class="text-primary view-file dropdown-item"  title="Click here to view matches" ><i class="fa fa-search"></i> Matches</a>
                                                            <a href="{{url('series/teams/'.$row->id)}}" class="text-primary view-file dropdown-item"  title="Click here to view teams" ><i class="fa fa-search"></i> Teams</a>
                                                        </div>
                                                    </div>
                                                </td>                                                                           
                                            </tr>
                                        @endforeach
                                    @else
                                    <tr><td colspan="3"><h5 style="text-align:center">No Record found!</h5></td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer text-center"> 
                            {{ $blade->data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection