@extends('layouts.index')
@section('main-content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-dark">
                        <div class="card-body p-0 table-responsive mailbox-messages">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="15%">Player Name</th>           
                                        <th width="5%" class="text-center">Country</th>  
                                        <th width="15%">Series</th>           
                                        <th width="5%" class="text-center">Matches</th>           
                                        <th width="5%" class="text-center">Wickets</th>           
                                        <th width="5%" class="text-center">Runs</th>           
                                        <th width="7%" class="text-center">Higest Score</th>           
                                        <th width="5%" class="text-center">50's</th>           
                                        <th width="5%" class="text-center">100's</th>           
                                        <th width="5%" class="text-center">6's</th>           
                                        <th width="5%" class="text-center">4's</th>           
                                        <th width="5%" class="text-center">Duck</th>         
                                        <th width="5%">Action</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($blade->data) && count($blade->data)>0 && $blade->data !=null)
                                        @foreach ($blade->data as $row)
                                            <tr>
                                                <td> @includeIf('root.comman.image',['dir'=>'players','file'=>Players(),'name'=>$row->players->FullName])</td> 
                                                <td class="text-center">@includeIf('root.comman.image',['dir'=>'logo','file'=>$row->series->teams->file])</td>
                                                <td>{{$row->series->title}}</td>
                                                <td class="text-center">{{$row->matches}}</td>
                                                <td class="text-center">{{$row->wickets}}</td>
                                                <td class="text-center">{{$row->runs}}</td>
                                                <td class="text-center">{{$row->highest_score}}</td>
                                                <td class="text-center">{{$row->fifties}}</td>
                                                <td class="text-center">{{$row->hundreds}}</td>
                                                <td class="text-center">{{$row->sixes}}</td>
                                                <td class="text-center">{{$row->fours}}</td>
                                                <td class="text-center">{{$row->duck}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="cursor:pointer;float:right">
                                                            <i class="fas fa-ellipsis-v"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" role="menu" style="">
                                                            <a href="javascript:void()" data-url="{{url('players/details/'.$row->id)}}" class="class-exploied text-primary view-file dropdown-item"  title="Click here to view" ><i class="fa fa-search"></i> View</a>
                                                        </div>
                                                    </div>
                                                </td>                                                                           
                                            </tr>
                                        @endforeach
                                    @else
                                    <tr><td colspan="3"><h5 style="text-align:center">No Record found!</h5></td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer text-center"> 
                            {{ $blade->data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection