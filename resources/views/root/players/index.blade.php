@extends('layouts.index')
@section('main-content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-dark">
                        <div class="card-body p-0 table-responsive mailbox-messages">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="15%">Team</th>
                                        <th width="10%">First Name</th>    
                                        <th width="35%">Last Name</th>    
                                        <th width="10%">Specialist</th>    
                                        <th width="5%" class="text-center">Jersey#</th>    
                                        <th width="5%">Action</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($blade->data) && count($blade->data)>0 && $blade->data !=null)
                                        @foreach ($blade->data as $row)
                                            <tr>
                                                <td>@includeIf('root.comman.image',['dir'=>'logo','file'=>$row->teams->file,'name'=>$row->teams->name])</td> 
                                                <td>{{ucfirst($row->first_name)}}</td>
                                                <td>
                                                    {{ucfirst($row->last_name)}}
                                                    @if($row->player->captain > 0)

                                                        <div style="float:right;">
                                                            @includeIf('root.comman.image',['dir'=>'captain','file'=>'captain.jpeg','name'=>'Captain'])
                                                        </div>
                                                        
                                                    @endif
                                                </td>
                                                <td>
                                                    @includeIf('root.comman.image',['dir'=>'specialist','file'=>$row->file,'name'=>ucwords(str_replace('_',' ',$row->specialist))])
                                                </td>
                                                <td class="text-center">{{$row->jersey}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="cursor:pointer;float:right">
                                                            <i class="fas fa-ellipsis-v"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" role="menu" style="">
                                                            <a href="javascript:void()" data-url="{{url('players/details/'.$row->id)}}" class="class-exploied text-primary view-file dropdown-item"  title="Click here to view" ><i class="fa fa-search"></i> View</a>
                                                        </div>
                                                    </div>
                                                </td>                                                                           
                                            </tr>
                                        @endforeach
                                    @else
                                    <tr><td colspan="3"><h5 style="text-align:center">No Record found!</h5></td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer text-center"> 
                            {{ $blade->data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection