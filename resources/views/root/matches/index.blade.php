@extends('layouts.index')
@section('main-content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-dark card-outline card-outline-tabs">
                        <div class="card-header p-0 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link text-dark @if($blade->active == 'fixtures') active @endif" id="fixtures-tab" href="{{url('matches/fixtures')}}" role="tab" aria-controls="fixtures" aria-selected="true">Fixtures</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark @if($blade->active == 'completed') active @endif" id="completed-tab" href="{{url('matches/completed')}}" role="tab" aria-controls="completed" aria-selected="false">Completed</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark @if($blade->active == 'cancelled') active @endif" id="cancelled-tab" href="{{url('matches/cancelled')}}" role="tab" aria-controls="cancelled" aria-selected="true">Cancelled</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark @if($blade->active == 'tie') active @endif" id="tie-tab" href="{{url('matches/tie')}}" role="tab" aria-controls="tie" aria-selected="true">Tied</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark @if($blade->active == 'no_result') active @endif" id="no_result-tab" href="{{url('matches/no_result')}}" role="tab" aria-controls="no_result" aria-selected="true">No Result</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body  p-0 table-responsive mailbox-messages">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="20%">Series</th>
                                        <th width="10%">Team-1</th>           
                                        <th width="10%">Team-2</th>           
                                        <th width="10%" class="text-center">Winner</th>           
                                        <th width="10%" class="text-center">Looser</th>
                                        <th width="10%" class="text-center">Score Team-1</th>           
                                        <th width="10%" class="text-center">Score Team-2</th>           
                                        <th width="10%" class="text-center">Date</th>  
                                        <th width="10%" class="text-center">Time</th>  
                                    </tr>
                                </thead>
                                <tbody>                                 
                                    @if(isset($blade->data) && count($blade->data)>0 && $blade->data !=null)
                                        @foreach ($blade->data as $row)
                                            <tr>
                                                <td>{{$row->series->title}}</td>  
                                                <td>{{ucfirst($row->teamone->name)}}</td>
                                                <td>{{ucfirst($row->teamtwo->name)}}</td>
                                                <td class="text-center text-success">                                                    
                                                    @if(is_null($row->winners))
                                                        N/A
                                                    @else
                                                        {{$row->winners->name}}
                                                    @endif
                                                </td>
                                                <td class="text-center text-danger">
                                                    @if(is_null($row->loosers) || $row->loosers == '0')
                                                        N/A
                                                    @else
                                                        {{$row->loosers->name}}
                                                    @endif
                                                </td>
                                                <td class="text-center text-success">{{$row->score_team_one}}</td>
                                                <td class="text-center text-danger">{{$row->score_team_two}}</td>
                                                <td class="text-center">{{$row->match_date}}</td>
                                                <td class="text-center">{{$row->match_time}}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                    <tr><td colspan="3"><h5 style="text-align:center">No Record found!</h5></td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer text-center"> 
                            {{ $blade->data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection