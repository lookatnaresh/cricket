
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="_token" content="{{csrf_token()}}" />
<title>{{env('APP_NAME')}} | Dashboard</title>
<link rel="stylesheet" href="{{ asset('root/plugins/fontawesome-free/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('root/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
<link rel="stylesheet" href="{{ asset('root/dist/css/adminlte.min.css') }}">
<link rel="stylesheet" href="{{ asset('root/plugins/toastr/toastr.min.css') }}">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">