<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{url('/')}}" class="brand-link">
        <strong class="brand-text font-weight-light ">{{env('APP_NAME')}}</strong>
    </a>
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">           
                <li class="nav-item ">
                    <a href="{{ url('teams') }}" class="nav-link @if($blade->menu == 'teams') active @endif "><i class="fas fa-caret-right nav-icon"></i> <p>Teams</p></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('points') }}" class="nav-link @if($blade->menu == 'points') active @endif "><i class="fas fa-caret-right nav-icon"></i> <p>Teams Points</p></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('players') }}" class="nav-link @if($blade->menu == 'players') active @endif "><i class="fas fa-caret-right nav-icon"></i><p>Players</p></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('history') }}" class="nav-link @if($blade->menu == 'history') active @endif "><i class="fas fa-caret-right nav-icon"></i><p>Players History</p></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('series') }}" class="nav-link @if($blade->menu == 'series') active @endif "><i class="fas fa-caret-right nav-icon"></i><p>All Series</p></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('matches') }}" class="nav-link @if($blade->menu == 'matches') active @endif "><i class="fas fa-caret-right nav-icon"></i><p>Matches</p></a>
                </li>
            </ul>
        </nav>
    </div>
</aside>