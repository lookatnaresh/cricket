
<script src="{{ asset('root/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('root/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('root/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('root/dist/js/adminlte.js') }}"></script>
<script src="{{ asset('root/dist/js/demo.js') }}"></script>
<script src="{{ asset('root/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('root/plugins/raphael/raphael.js') }}"></script>
<script src="{{ asset('root/plugins/jquery-mapael/jquery.mapael.js') }}"></script>
<script src="{{ asset('root/plugins/jquery-mapael/maps/usa_states.js') }}"></script>
<script src="{{ asset('root/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('root/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('root/js/players.js') }}"></script>
@stack('BScript')

<div class="modal fade" id="loadModal" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content load-modal-content"></div>
    </div>
</div>