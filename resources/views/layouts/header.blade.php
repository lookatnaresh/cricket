<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li> 
        @include('root.comman.breadcrumb')
    </ul>
   
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <img src="{{asset('root/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" style="width:20px;height:20px" alt="User Image">
            </a>
        </li>
    </ul>
  </nav>