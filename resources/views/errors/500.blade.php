@extends('layouts.index')
@section('main-content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-dark">
                        <div class="card-header">Error 404  </div>
                        <div class="card-body p-5 table-responsive mailbox-messages">
                            <div class="error-page p-5 ">
                                <h1 class="headline text-warning"> 404</h1>
                                <div class="error-content">
                                    <h2><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h2>
                                    <p>
                                        We could not find the page you were looking for.
                                        Meanwhile, you may click here to go <a href="javascript:void(0)" onclick="window.history.back();"> Back</a>.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
@endsection