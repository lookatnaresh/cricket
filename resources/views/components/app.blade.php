<!DOCTYPE html>
<html>
    <head>
        @include('layouts.head')
    </head>
    <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
        <div class="wrapper">
            
            @include('layouts.header')

            @include('layouts.nav')

            <div class="content-wrapper overflow-auto" style="padding-top:1%;max-height:400px">

                @yield('main-content')
                
            </div>
            
            <footer class="main-footer">
                <strong>Copyright &copy; 2014-{{date('Y')}} </strong> All rights reserved.
                <div class="float-right d-none d-sm-inline-block"><b>Version</b> 3.0.0</div>
            </footer>
            
        </div>

        @include('layouts.footer')

    </body>
</html>