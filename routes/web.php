<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Root'], function () {     
    Route::get('/',"CricketTeamsController@operate");
    Route::get("teams/{func?}/{id?}","CricketTeamsController@operate");
    Route::get("series/{func?}/{id?}/{match?}","CricketSeriesController@operate");
    Route::get("points/{func?}/{id?}","CricketTeamsPointsController@operate");
    Route::get("players/{func?}/{id?}","CricketPlayersController@operate");
    Route::get("history/{func?}/{id?}","CricketPlayersHistoryController@operate");
    Route::get("matches/{func?}/{id?}","CricketSeriesMatchesController@operate");
});
