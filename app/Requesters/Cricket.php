<?php

namespace App\Requesters;
use DB;

class Cricket {

    private $countries      =   []; 
    private $founded        =   [];
    private $sname          =   [];
    private $logo           =   [];
    private $tours          =   [];
    private $specialist     =   [];
    private $players        =   [];
    private $status         =   [];

    public function __construct(){
    
        $this->countries        =   ['India','Bangladesh','Sri Lanka','Pakistan','Australia','West Indies','New Zealand','England']; 
        $this->founded          =   ['1792','1999','1975','1949', '1877','1890','1930','1830'];
        $this->specialist       =   ['batsman','all_rounder','wicket_keeper','bowler'];
        $this->sname            =   ['IND','BAN','SRL','PAK','AUS','WIN','NEZ','ENG'];
        $this->logo             =   ['ind.png','ban.png','srl.jpeg','pak.png','aus.png','win.png','nez.png','eng.png'];
        $this->status           =   ['completed','cancelled','fixtures','tie','no_result'];

        $this->tours            =   [
                                        'India tour of New Zealand',
                                        'Sri Lanka tour of India',
                                        'Australia tour of Sri Lanka',
                                        'Pakistan tour of Australia',
                                        'ODI Series',
                                        'Twenty20 Series',
                                        'Australia tour of England',
                                        'New Zealand tour of West Indies',
                                    ];

        

        $this->players          =   [
                                        '1'=>[
                                            'Prithvi'=>'Shaw','Mayank'=>'Agarwal','Navdeep'=>'Saini','Shivam'=>'Dube','Shubman'=>'Gill','Vijay'=>'Shankar',
                                            'Mohammed'=>'Siraj','Rishabh'=>'Pant','Deepak'=>'Chahar','Khaleel'=>'Ahmed','Siddarth'=>'Kaul','Washington'=>'Sundar',
                                            'Shreyas'=>'Iyer','Shardul'=>'Thakur','Kuldeep'=>'Yadav','Jayant'=>'Yadav','Hardik'=>'Pandya','Faiz'=>'Fazal'
                                        ],
                                        '2'=>[
                                            'Mohammad'=>'Naim','Afif'=>'Hossain','Abu'=>'Jayed','Ariful'=>'Haque','Fazle'=>'Mahmud','Nazmul'=>'Islam',
                                            'Najmul'=>'Hossain Shanto','Abu'=>'Hider','Mohammad'=>'Saifuddin','Sunzamul'=>'Islam','Mehidy'=>'Hasan','Tanbir'=>'Hayder',
                                            'Subashis'=>'Roy','Nurul'=>'Hasan','Mosaddek'=>'Hossain','Mustafizur'=>'Rahman','Liton'=>'Das','Taijul'=>'Islam'
                                        ],
                                        '3'=>[
                                            'Minod'=>'Bhanuka','Priyamal'=>'Perera','Kamindu'=>'Mendis','Oshada'=>'Fernando','Kasun'=>'Rajitha','Prabath'=>'Jayasuriya',
                                            'Shehan'=>'Madushanka','Sadeera'=>'Samarawickrama','Malinda'=>'Pushpakumara','Dilshan'=>'Munaweera','Vishwa'=>'Fernando','Asitha'=>'Fernando',
                                            'Wanindu'=>'Hasaranga','Lahiru'=>'Madushanka','Lahiru'=>'Kumara','Sandun'=>'Weerakkody','Asela'=>'Gunaratne','Avishka'=>'Fernando'
                                        ],
                                        '4'=>[
                                            'Saad'=>'Ali','Abid'=>'Ali','Mohammad'=>'Hasnain','Shan'=>'Masood','Mohammad'=>'Abbas','Hussain'=>'Talat',
                                            'Shaheen'=>'Afridi','Asif'=>'Ali','Usman'=>'Shinwari','Rumman'=>'Raees','Faheem'=>'Ashraf','Fakhar'=>'Zaman',
                                            'Shadab'=>'Khan','Mohammad'=>'Nawaz','Hasan'=>'Ali','Zafar'=>'Gohar','Iftikhar'=>'Ahmed','Bilal'=>'Asif'
                                        ],
                                        '5'=>[
                                            'Julien'=>'Wiener','Bruce'=>'Laird','Kevin'=>'Wright','Wally'=>'Edwards','Steve'=>'Smith','Alan'=>'Hurst',
                                            'Rick'=>'McCosker','Alan'=>'Turner','Richie'=>'Robinson','Bob'=>'Simpson','Peter'=>'Toohey','Graham'=>'Yallop',
                                            'Steve'=>'Rixon','Allan'=>'Border','Phil'=>'Carlson','John'=>'Maclean','Rodney'=>'Hogg','Gary'=>'Cosier'
                                        ],
                                        '6'=>[
                                            'Khary'=>'Pierre','Brandon'=>'King','Hayden'=>'Walsh Jr.','Romario'=>'Shepherd','Raymon'=>'Reifer','Shane'=>'Dowrich',
                                            'Nicholas'=>'Pooran','John'=>'Campbell','Fabian'=>'Allen','Obed'=>'McCoy','Oshane'=>'Thomas','Chandrapaul'=>'Hemraj',
                                            'Keemo'=>'Paul','Shimron'=>'Hetmyer','Ronsford'=>'Beaton','Sunil'=>'Ambris','Kesrick'=>'Williams','Kyle'=>'Hope'
                                        ],
                                        '7'=>[
                                            'Kyle'=>'Jamieson','Tom'=>'Blundell','Tim'=>'Seifert','Mark'=>'Chapman','Todd'=>'Astle','Seth'=>'Rance',
                                            'Scott'=>'Kuggeleijn','Lockie'=>'Ferguson','Henry'=>'Nicholls','George'=>'Worker','Ish'=>'Sodhi','Andrew'=>'Mathieson',
                                            'Ben'=>'Wheeler','Mitchell'=>'Santner','Matt'=>'Henry','Anton'=>'Devcich','Corey'=>'Anderson','Luke'=>'Ronchi'
                                        ],
                                        '8'=>[
                                            'Saqib'=>'Mahmood','Matt'=>'Parkinson','Tom'=>'Banton','Dawid'=>'Malan','Ben'=>'Foakes','Jofra'=>'Archer',
                                            'Olly'=>'Stone','Sam'=>'Curran','Craig'=>'Overton','Tom'=>'Curran','Toby'=>'Roland Jones','Ben'=>'Duckett',
                                            'Jake'=>'Ball','Liam'=>'Dawson','Reece'=>'Topley','Sam'=>'Billings','Mark'=>'Wood','David'=>'Willey'
                                        ]
                                    ];
    }

    private function cricket_teams(){
        $data = [];

        foreach($this->countries as $key=>$country){
            $data[] = ['name'=>$country,'founded'=>$this->founded[$key],'short_name'=>$this->sname[$key],'file'=>$this->logo[$key],'created_at'=> CDate(),'updated_at'=> CDate()];
        }
        return $data;
        /*
        return  [
            ['name'=>$this->countries[0],'founded'=>$this->founded[0],'short_name'=>$this->sname[0],'file'=>$this->logo[0],'created_at'=> CDate(),'updated_at'=> CDate()],
            ['name'=>$this->countries[1],'founded'=>$this->founded[1],'short_name'=>$this->sname[1],'file'=>$this->logo[1],'created_at'=> CDate(),'updated_at'=> CDate()],
            ['name'=>$this->countries[2],'founded'=>$this->founded[2],'short_name'=>$this->sname[2],'file'=>$this->logo[2],'created_at'=> CDate(),'updated_at'=> CDate()],
            ['name'=>$this->countries[3],'founded'=>$this->founded[3],'short_name'=>$this->sname[3],'file'=>$this->logo[3],'created_at'=> CDate(),'updated_at'=> CDate()],
            ['name'=>$this->countries[4],'founded'=>$this->founded[4],'short_name'=>$this->sname[4],'file'=>$this->logo[4],'created_at'=> CDate(),'updated_at'=> CDate()],
            ['name'=>$this->countries[5],'founded'=>$this->founded[5],'short_name'=>$this->sname[5],'file'=>$this->logo[5],'created_at'=> CDate(),'updated_at'=> CDate()],
            ['name'=>$this->countries[6],'founded'=>$this->founded[6],'short_name'=>$this->sname[6],'file'=>$this->logo[6],'created_at'=> CDate(),'updated_at'=> CDate()],
            ['name'=>$this->countries[7],'founded'=>$this->founded[7],'short_name'=>$this->sname[7],'file'=>$this->logo[7],'created_at'=> CDate(),'updated_at'=> CDate()],
        ];
        */
    }

    private function cricket_series(){
        return [
            ['title'=>$this->tours [0],'tour_year'=>SYears(),'cricket_teams_id'=>7,'type'=>'oneday','created_at'=> CDate(),'updated_at'=> CDate()],
            ['title'=>$this->tours [1],'tour_year'=>SYears(),'cricket_teams_id'=>1,'type'=>'oneday','created_at'=> CDate(),'updated_at'=> CDate()],
            ['title'=>$this->tours [2],'tour_year'=>SYears(),'cricket_teams_id'=>3,'type'=>'test','created_at'=> CDate(),'updated_at'=> CDate()],
            ['title'=>$this->tours [3],'tour_year'=>SYears(),'cricket_teams_id'=>5,'type'=>'oneday','created_at'=> CDate(),'updated_at'=> CDate()],
            ['title'=>$this->tours [4],'tour_year'=>SYears(),'cricket_teams_id'=>4,'type'=>'oneday','created_at'=> CDate(),'updated_at'=> CDate()],
            ['title'=>$this->tours [5],'tour_year'=>SYears(),'cricket_teams_id'=>5,'type'=>'T20','created_at'=> CDate(),'updated_at'=> CDate()],
            ['title'=>$this->tours [6],'tour_year'=>SYears(),'cricket_teams_id'=>8,'type'=>'oneday','created_at'=> CDate(),'updated_at'=> CDate()],
            ['title'=>$this->tours [7],'tour_year'=>SYears(),'cricket_teams_id'=>7,'type'=>'oneday','created_at'=> CDate(),'updated_at'=> CDate()],
        ];
    }

    private function cricket_teams_points(){
        $data = [];
        $CTeams = DB::table('cricket_teams')->get();
        foreach($CTeams as $row){
            $data[]=[
                'cricket_teams_id'  => $row->id,
                'points'            => rand(1000,5000),
                'won'               => rand(100,500),
                'lost'              => rand(100,300),
                'tied'              => rand(50,200),
                'noresult'          => rand(50,200),
                'created_at'        => CDate(),
                'updated_at'        => CDate()
            ];
        }
        return $data;
    }

    private function cricket_players(){
        $data = [];
        
        foreach($this->players as $key => $player){
            foreach($player as $first=>$last){
                shuffle($this->specialist);
                $data[] = [
                    'first_name'        => $first,
                    'last_name'         => $last,
                    'file'              => $this->specialist[0].'.jpeg',
                    'specialist'        => $this->specialist[0],
                    'cricket_teams_id'  => $key,
                    'jersey'            => rand(10,100),
                    'created_at'        => CDate(),
                    'updated_at'        => CDate()
                ];
            }
        }

        return $data;
    }

    private function cricket_players_histories(){
        $data = [];

        $cricketSeries = DB::table('cricket_series')->get();
        foreach($cricketSeries as $series){
            $cricketTeams = DB::table('cricket_series_teams')->where('cricket_series_id',$series->id)->get();
            foreach($cricketTeams as $team){
                $cricketPlayers = DB::table('cricket_teams_players')->where('cricket_teams_id',$team->cricket_teams_id)->get();
                foreach($cricketPlayers as $player){
                    $data[]=[
                        'cricket_series_id' => $series->id,
                        'cricket_players_id'=> $player->cricket_players_id,
                        'matches'           => rand(1,10),
                        'wickets'           => rand(1,50),
                        'runs'              => rand(100,2000),
                        'highest_score'     => rand(50,200),
                        'fifties'           => rand(0,10),
                        'hundreds'          => rand(0,10),
                        'fours'             => rand(0,50),
                        'sixes'             => rand(0,50),
                        'duck'              => rand(0,10),
                        'created_at'        => CDate(),
                        'updated_at'        => CDate() 
                    ];
                }
            }
        }

        return $data;
    }

    private function cricket_teams_players(){
        $data = [];

        $cricketTeams = DB::table('cricket_teams')->get();
        foreach($cricketTeams as $team){
            $cricketPlayers = DB::table('cricket_players')->where('cricket_teams_id',$team->id)->get();
            $captain = rand(1,12);                
            foreach($cricketPlayers as $key => $player ){
                $data[] = [
                        'cricket_teams_id'      => $team->id,
                        'cricket_players_id'    => $player->id,
                        'captain'               => ($key == $captain) ? $captain : 0,
                        'created_at'            => CDate(),
                        'updated_at'            => CDate()
                    ];
            }
        }

        return $data;
    }

    private function cricket_series_teams(){
        $data = [];
        $cricketSeries = DB::table('cricket_series')->get();
        foreach($cricketSeries as $series){
            $rands = rand(2,8);
            for($i=1;$i<=$rands;$i++){
                $data[] = [
                            'cricket_series_id' => $series->id,
                            'cricket_teams_id'  => $i,
                            'created_at'        => CDate(),
                            'updated_at'        => CDate() 
                        ];
            }
        }
        return $data;
    }

    private function cricket_series_matches(){
        $data = [];
        $cricketSeries = DB::table('cricket_series')->get();
        foreach($cricketSeries as $series){
            $rands = rand(3,6);
            for($i=1;$i<=$rands;$i++){
                shuffle($this->status);

                $score  = $this->score();
                $teams  = $this->teams();   
                $points = $this->points();
                
                if($score['one'] > $score['two']){
                    $winner = $teams['one'];
                    $looser = $teams['two'];
                }
                if($score['one'] < $score['two']){
                    $looser = $teams['one'];
                    $winner = $teams['two'];
                }

                $arr1 = ['cricket_series_id'=>$series->id,'team_one'=>$teams['one'],'team_two'=>$teams['two']];

                if($this->status[0] == 'fixtures' || $this->status[0] == 'cancelled'){
                    $arr2 = ['score_team_one'=>'0','score_team_two'=>'0','winner'=>'0','loser' =>'0','points_team_one'=>'0','points_team_two' =>'0'];
                }else{
                    if($this->status[0] == 'tie'){
                        $arr2 = ['score_team_one'=>$score['one'],'score_team_two'=>$score['one'],'winner'=>0,'loser'=>0,'points_team_one'=>$points['one'],'points_team_two'=>$points['two']];
                    }elseif($this->status[0] == 'no_result'){
                        $arr2 = ['score_team_one'=>$score['one'],'score_team_two'=>$score['two'],'winner'=>0,'loser'=>0,'points_team_one'=>$points['one'],'points_team_two'=>$points['two']];
                    }else{
                        $arr2 = ['score_team_one'=>$score['one'],'score_team_two'=>$score['two'],'winner'=>$winner,'loser'=>$looser,'points_team_one'=>$points['one'],'points_team_two'=>$points['two']];
                    }
                }

                $arr3   = ['status' => $this->status[0],'match_date'=> date("Y-m-d",rand(1421984549,1561984549)),'match_time'=> rand(1,23).':00:00','created_at'=> CDate(),'updated_at'=> CDate() ];
            
                $data[] = array_merge($arr1,$arr2,$arr3);
            }
        }
        return $data;

    }

    private function score(){
        $score_one = rand(180,400);
        $score_two = rand(180,400);
        return ['one'=>$score_one,'two'=>$score_two];
    }

    private function teams(){

        $team_one = rand(1,8);
        $team_two = rand(1,8);

        if($team_one == $team_two) {
            return ['one'=>rand(1,8),'two'=>rand(1,8)];
        }else{
            return ['one'=>$team_one,'two'=>$team_two];
        }
        
    }

    private function points(){

        $point_one = rand(-2,3);
        $point_two = rand(-2,3);

        if($point_one == $point_two){
            return ['one'=>rand(-2,3),'two'=>rand(-1,2)];
        }else{
            return ['one'=>$point_one,'two'=>$point_two];
        }
    }

    public function migrate($parms){
        DB::table($parms)->insert($this->{$parms}());
    }

}
