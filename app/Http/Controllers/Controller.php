<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController{
    
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $cricket     =   null;
    public $id          =   null;
    public $request     =   null;
    public $base        =   "base";
    public $blade       =   "blade";
    public $index       =   "index";

    public function __construct(){
        $this->cricket = new \stdClass;
    }

}
