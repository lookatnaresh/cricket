<?php

namespace App\Http\Controllers\Root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Root\CricketTeamsPoint;

class CricketTeamsPointsController extends Controller{
    private $file       =   null;
    private $filepath   =   null;
    private $title      =   "Points";
    private $route      =   "points/";
    private $view       =   "root/points/";

    public function __construct(){
        parent::__construct();
        $this->cricket->button       = true;
        $this->cricket->base         = $this->route;
        $this->cricket->menu         = 'points';
        $this->cricket->breadcrumb   = [$this->title];
    }

    public function operate(Request $request,$func='index',$id =null){
        $this->id       = $id;
        $this->request  = $request;
        return $this->{$func}();
    }

    public function index(){
        $this->cricket->modal    =   true;
        $this->cricket->title    =   $this->title;
        $this->cricket->data     =   CricketTeamsPoint::with('teams:id,name,file')->paginate(10);
        //dd($this->cricket->data);
        return view($this->view.$this->index)->with($this->blade,$this->cricket);
    }
}
