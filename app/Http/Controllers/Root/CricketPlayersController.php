<?php
namespace App\Http\Controllers\Root;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Root\{CricketPlayer,CricketPlayersHistory,CricketTeamsPlayer};
use Illuminate\Support\Facades\View;

class CricketPlayersController extends Controller{

    private $file       =   null;
    private $filepath   =   null;
    private $title      =   "Players";
    private $route      =   "players/";
    private $view       =   "root/players/";

    public function __construct(){
        parent::__construct();
        $this->cricket->button       = true;
        $this->cricket->base         = $this->route;
        $this->cricket->menu         = 'players';
        $this->cricket->breadcrumb   = [$this->title];
    }

    public function operate(Request $request,$func='index',$id =null){
        $this->id       = $id;
        $this->request  = $request;
        return $this->{$func}();
    }

    public function index(){
        $this->cricket->modal    =   true;
        $this->cricket->title    =   $this->title;
        $this->cricket->data     =   CricketPlayer::with(['teams:id,name,file','player'])->paginate(10);
        return view($this->view.$this->index)->with($this->blade,$this->cricket);
    }

    public function details(){      
        $player                  = CricketPlayersHistory::find($this->id);
        $this->cricket->title    = $this->title; 
        $this->cricket->basic    = CricketPlayer::with('teams')->where('id',$player->cricket_players_id)->first(); 
        $this->cricket->history  = CricketPlayersHistory::with(['series.teams'])->where('cricket_players_id',$player->cricket_players_id)->get(); 

        return json_encode(['status'=>200,'success'=>true,'html'=>View::make('root/modal/player-details',[$this->blade=>$this->cricket])->render()]);   

    }

}
