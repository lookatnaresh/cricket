<?php

namespace App\Http\Controllers\Root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Root\CricketSeriesMatch;

class CricketSeriesMatchesController extends Controller{
    private $parm       =   null;
    private $file       =   null;
    private $filepath   =   null;
    private $title      =   "Matches";
    private $route      =   "match/";
    private $view       =   "root/matches/";

    public function __construct(){
        parent::__construct();
        $this->cricket->button       = true;
        $this->cricket->base         = $this->route;
        $this->cricket->menu         = 'matches';
        $this->cricket->breadcrumb   = [$this->title];
    }

    public function operate(Request $request,$func='index',$id =null){
        $this->id       = $id;
        $this->parm     = ($func == 'index') ?'fixtures' : $func;
        $this->request  = $request;
        return $this->{$func}();
    }

    public function index(){
        $this->cricket->modal    =   true;
        $this->cricket->title    =   $this->title;
        $this->cricket->active   =   $this->parm;
        $this->cricket->data     =   CricketSeriesMatch::with(['series:id,title','teamone:id,name','teamtwo:id,name','loosers:id,name','winners:id,name'])->where('status',$this->parm)->paginate(10);
        return view($this->view.$this->index)->with($this->blade,$this->cricket);
    }
    
    private function fixtures(){
        return $this->index();
    }
    
    private function completed(){
        return $this->index();
    }
    
    private function cancelled(){
        return $this->index();
    }
    
    private function tie(){
        return $this->index();
    }
    
    private function no_result(){
        return $this->index();
    }
}
