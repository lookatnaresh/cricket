<?php

namespace App\Http\Controllers\Root;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Root\{CricketTeam,CricketPlayer};

class CricketTeamsController extends Controller {
    
    private $file       =   null;
    private $filepath   =   null;
    private $title      =   "Teams";
    private $route      =   "teams/";
    private $view       =   "root/";

    public function __construct(){
        parent::__construct();
        $this->cricket->button       = true;
        $this->cricket->base         = $this->route;
        $this->cricket->menu         = 'teams';
        $this->cricket->breadcrumb   = [$this->title];
    }

    public function operate(Request $request,$func='index',$id =null){
        $this->id       = $id;
        $this->request  = $request;
        return $this->{$func}();
    }

    public function index(){
        $this->cricket->modal    =   true;
        $this->cricket->title    =   $this->title;
        $this->cricket->data     =   CricketTeam::paginate(10);
        return view($this->view.'teams/'.$this->index)->with($this->blade,$this->cricket);
    }

    public function players(){
        $this->cricket->modal        =   true;
        $this->cricket->title        =   $this->title;
        $this->cricket->country      =   CricketTeam::find($this->id);
        
        $this->cricket->breadcrumb   =   [$this->cricket->country->name,$this->title,'Players'];
        $this->cricket->data         =   CricketPlayer::with('teams:id,name,file')->where('cricket_teams_id',$this->id)->paginate(10);

        return view($this->view.'players/'.$this->index)->with($this->blade,$this->cricket);
    }

}
