<?php
namespace App\Http\Controllers\Root;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Root\{CricketSeries,CricketSeriesTeam,CricketSeriesMatch};

class CricketSeriesController extends Controller{

    private $file       =   null;
    private $filepath   =   null;
    private $title      =   "Series";
    private $route      =   "series/";
    private $view       =   "root/";

    public function __construct(){
        parent::__construct();
        $this->cricket->button       = true;
        $this->cricket->base         = $this->route;
        $this->cricket->menu         = 'series';
        $this->cricket->breadcrumb   = [$this->title];
    }

    public function operate(Request $request,$func='index',$id =null,$match='fixtures'){
        $this->id       = $id;
        $this->request  = $request;
        $this->parm     = $match;
        return $this->{$func}();
    }

    public function index(){
        $this->cricket->modal    =   true;
        $this->cricket->title    =   $this->title;
        $this->cricket->data     =   CricketSeries::with('teams:id,name,file')->paginate(10);
        return view($this->view.'series/'.$this->index)->with($this->blade,$this->cricket);
    }

    public function teams(){
        $this->cricket->modal        =   true;
        $this->cricket->title        =   $this->title;
        $this->cricket->series       =   CricketSeries::find($this->id);
        $this->cricket->breadcrumb   =   [$this->title,$this->cricket->series->title,'Teams'];
        $this->cricket->data         =   CricketSeriesTeam::with('teams')->where('cricket_series_id',$this->id)->paginate(10);
        return view($this->view.'series/teams')->with($this->blade,$this->cricket);
    }

    public function matches(){
        $this->cricket->modal       =   true;
        $this->cricket->seriesid    =   $this->id;
        $this->cricket->title       =   $this->title;
        $this->cricket->active      =   $this->parm;
        $this->cricket->series      =   CricketSeries::find($this->id);
        $this->cricket->breadcrumb  =   [$this->title,$this->cricket->series->title,'Matches'];
        $this->cricket->data        =   CricketSeriesMatch::with(['series:id,title','teamone:id,name','teamtwo:id,name','loosers:id,name','winners:id,name'])->
                                                         where('cricket_series_id',$this->id)->
                                                         where('status',$this->parm)->
                                                         paginate(10);
        
        return view($this->view.'series/matches')->with($this->blade,$this->cricket);
    }

}
