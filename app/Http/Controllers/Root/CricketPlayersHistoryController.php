<?php

namespace App\Http\Controllers\Root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Root\CricketPlayersHistory;

class CricketPlayersHistoryController extends Controller{
    private $file       =   null;
    private $filepath   =   null;
    private $title      =   "Players History";
    private $route      =   "history/";
    private $view       =   "root/history/";

    public function __construct(){
        parent::__construct();
        $this->cricket->button       = true;
        $this->cricket->base         = $this->route;
        $this->cricket->menu         = 'history';
        $this->cricket->breadcrumb   = [$this->title];
    }

    public function operate(Request $request,$func='index',$id =null){
        $this->id       = $id;
        $this->request  = $request;
        return $this->{$func}();
    }

    public function index(){
        $this->cricket->modal    =   true;
        $this->cricket->title    =   $this->title;
        $this->cricket->data     =   CricketPlayersHistory::with(['players:id,first_name,last_name,file','series.teams'])->paginate(10);
        //dd($this->cricket->data);
        return view($this->view.$this->index)->with($this->blade,$this->cricket);
    }
}
