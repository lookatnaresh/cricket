<?php

    use Illuminate\Support\Facades\Crypt;
    use App\Requesters\Cricket;
    use Carbon\Carbon;

    if (!function_exists('migrate')) {
        function migrate($parms) {   
            (new Cricket)->migrate($parms);
        }		
    }

    if (!function_exists('CDate')) {
        function CDate() {   
            return Carbon::now();
        }		
    }

    if (!function_exists('SYears')) {
        function SYears() {   
            return rand(2000,2015);
        }		
    }

    if (!function_exists('Players')) {
        function Players() {  
            $arr = ['avatar-1.png','avatar-2.png','avatar-3.png','avatar-4.jpg','avatar-5.jpg','avatar-6.jpg'] ;
            shuffle($arr);
            return $arr[0];
        }		
    }

    if (!function_exists('encrypted')) {
		function encrypted($value) {
            return Crypt::encryptString($value);
		}		
	}

    if (!function_exists('decrypted')) {
		function decrypted($value) {
            return Crypt::decryptString($value);
		}		
	}

?>