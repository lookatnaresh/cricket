<?php

namespace App\Models\Root;

use Illuminate\Database\Eloquent\Model;

class CricketPlayersHistory extends Model{

    public function players() {
        return $this->belongsTo(CricketPlayer::class,'cricket_players_id');
    }
    
    public function series() {
        return $this->belongsTo(CricketSeries::class,'cricket_series_id');
    }

}
