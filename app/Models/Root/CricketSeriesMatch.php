<?php

namespace App\Models\Root;

use Illuminate\Database\Eloquent\Model;

class CricketSeriesMatch extends Model{

    public function series() {
        return $this->belongsTo(CricketSeries::class,'cricket_series_id');
    }

    public function teamone() {
        return $this->belongsTo(CricketTeam::class,'team_one');
    }
    
    public function teamtwo() {
        return $this->belongsTo(CricketTeam::class,'team_two');
    }
    
    public function winners() {
        return $this->belongsTo(CricketTeam::class,'winner');
    }
    
    public function loosers() {
        return $this->belongsTo(CricketTeam::class,'loser');
    }
    
}
