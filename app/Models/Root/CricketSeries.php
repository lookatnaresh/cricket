<?php

namespace App\Models\Root;

use Illuminate\Database\Eloquent\Model;

class CricketSeries extends Model{
    public function teams() {
        return $this->belongsTo(CricketTeam::class,'cricket_teams_id');
    }
    
}
