<?php

namespace App\Models\Root;

use Illuminate\Database\Eloquent\Model;

class CricketPlayer extends Model{

    public function teams() {
        return $this->belongsTo(CricketTeam::class,'cricket_teams_id');
    }

    public function player() {
        return $this->hasOne(CricketTeamsPlayer::class,'cricket_players_id');
    }

    public function getFullNameAttribute(){
        return "{$this->first_name} {$this->last_name}";
    }
    
}
