<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCricketPlayersHistoriesTable extends Migration{

    public function up(){
        Schema::create('cricket_players_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cricket_series_id')->unsigned()->index();
            $table->foreign('cricket_series_id')->references('id')->on('cricket_series')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('cricket_players_id')->unsigned()->index();
            $table->foreign('cricket_players_id')->references('id')->on('cricket_players')->onDelete('cascade')->onUpdate('cascade');
            //$table->unique(['cricket_series_id','cricket_players_id']);
            $table->integer('matches')->default(0);
            $table->integer('wickets')->default(0);
            $table->integer('five_wickets')->default(0);
            $table->integer('ten_wickets')->default(0);
            $table->integer('runs')->default(0);
            $table->integer('highest_score')->default(0);
            $table->integer('fifties')->default(0);
            $table->integer('hundreds')->default(0);
            $table->integer('fours')->default(0);
            $table->integer('sixes')->default(0);
            $table->integer('duck')->default(0);
            $table->timestamps();
        });

        migrate('cricket_players_histories');
    }

    public function down(){
        Schema::dropIfExists('cricket_players_histories');
    }
}
