<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCricketSeriesTeamsTable extends Migration{

    public function up(){
        Schema::create('cricket_series_teams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cricket_series_id')->unsigned()->index();
            $table->bigInteger('cricket_teams_id')->unsigned()->index();
            $table->foreign('cricket_series_id')->references('id')->on('cricket_series')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('cricket_teams_id')->references('id')->on('cricket_teams')->onDelete('cascade')->onUpdate('cascade');
            //$table->unique(['cricket_series_id','cricket_teams_id']);
            $table->enum('is_active',[0,1])->default(1);
            $table->text('remarks')->nullable();
            $table->timestamps();
        });

        migrate('cricket_series_teams');
    }

    public function down(){
        Schema::dropIfExists('cricket_series_teams');
    }
}
