<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Requesters\Cricket;

class CreateCricketSeriesTable extends Migration {
    
    public function up(){
        Schema::create('cricket_series', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->string('tour_year');
            $table->string('title')->unique();
            $table->bigInteger('cricket_teams_id')->unsigned()->index();
            $table->foreign('cricket_teams_id')->references('id')->on('cricket_teams')->onDelete('cascade')->onUpdate('cascade');
            //$table->string('country');
            $table->enum('type',['oneday','test','T20'])->default('oneday');
            $table->enum('is_active',[0,1])->default(1);
            $table->text('description')->nullable();
            $table->timestamps();

        });

        migrate('cricket_series');

    }

    public function down(){
        Schema::dropIfExists('cricket_series');
    }
}
