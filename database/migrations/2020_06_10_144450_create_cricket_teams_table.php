<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCricketTeamsTable extends Migration{
    
    public function up(){
        Schema::create('cricket_teams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('founded');
            $table->string('name');
            $table->string('short_name');
            $table->string('file');
            $table->text('description')->nullable();
            $table->enum('is_active',[0,1])->default(1);
            $table->timestamps();
        });

        migrate('cricket_teams');
    }

    
    public function down(){
        Schema::dropIfExists('cricket_teams');
    }
}
