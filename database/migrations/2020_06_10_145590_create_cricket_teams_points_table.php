<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCricketTeamsPointsTable extends Migration {
    
    public function up(){
        Schema::create('cricket_teams_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unique('cricket_teams_id');
            $table->bigInteger('cricket_teams_id')->unsigned()->index();
            $table->foreign('cricket_teams_id')->references('id')->on('cricket_teams')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('won')->default(0);
            $table->bigInteger('lost')->default(0);
            $table->bigInteger('tied')->default(0);
            $table->bigInteger('noresult')->default(0);
            $table->bigInteger('points')->default(0);
            $table->timestamps();
        });

        migrate('cricket_teams_points');

    }

    
    public function down(){
        Schema::dropIfExists('cricket_teams_points');
    }
}
