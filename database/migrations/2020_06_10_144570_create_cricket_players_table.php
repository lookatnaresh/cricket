<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCricketPlayersTable extends Migration{

    public function up(){
        Schema::create('cricket_players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('file')->nullable();
            $table->enum('specialist',['batsman','bowler','all_rounder','wicket_keeper']);
            $table->integer('jersey')->nullable();
            $table->bigInteger('cricket_teams_id')->unsigned()->index();
            $table->foreign('cricket_teams_id')->references('id')->on('cricket_teams')->onDelete('cascade')->onUpdate('cascade');
            $table->enum('is_active',[0,1])->default(1);
            $table->text('description')->nullable();
            $table->timestamps();            
        });

        migrate('cricket_players');
    }

    public function down(){
        Schema::dropIfExists('cricket_players');
    }
}
