<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCricketTeamsPlayersTable extends Migration{
    
    public function up(){
        Schema::create('cricket_teams_players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('captain')->default(0);
            $table->bigInteger('cricket_teams_id')->unsigned()->index();
            $table->bigInteger('cricket_players_id')->unsigned()->index();
            $table->foreign('cricket_teams_id')->references('id')->on('cricket_teams')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('cricket_players_id')->references('id')->on('cricket_players')->onDelete('cascade')->onUpdate('cascade');
            $table->integer(['cricket_teams_id','cricket_players_id']);
            $table->timestamps();
        });

        migrate('cricket_teams_players');
    }

    
    public function down(){
        Schema::dropIfExists('cricket_teams_players');
    }
}
