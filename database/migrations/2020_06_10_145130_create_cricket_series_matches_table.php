<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCricketSeriesMatchesTable extends Migration{
    
    public function up(){
        Schema::create('cricket_series_matches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cricket_series_id')->unsigned()->index();
            $table->bigInteger('team_one')->unsigned()->nullable()->index();
            $table->bigInteger('team_two')->unsigned()->nullable()->index();
            $table->foreign('cricket_series_id')->references('id')->on('cricket_series')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('team_one')->references('id')->on('cricket_teams')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('team_two')->references('id')->on('cricket_teams')->onDelete('cascade')->onUpdate('cascade');
            $table->string('winner')->nullable();
            $table->string('loser')->nullable();
            $table->string('score_team_one')->nullable()->default(0);
            $table->string('score_team_two')->nullable()->default(0); 
            $table->string('points_team_one')->nullable()->default(0);
            $table->string('points_team_two')->nullable()->default(0); 
            $table->enum('status',['completed','cancelled','fixtures','tie','no_result'])->default('fixtures');
            $table->date('match_date');
            $table->time('match_time');
            $table->timestamps();
        });

        migrate('cricket_series_matches');

    }

    
    public function down(){
        Schema::dropIfExists('cricket_series_matches');
    }
}
