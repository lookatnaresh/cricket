
    var $validate = null;
    $(document).ready(function(){

        $(document).on('click','.class-exploied',function(event){
            ajaxCall('GET',$(this).attr('data-url'),null,function(result){
                if(result.success==true){
                    $('.load-modal-content').html("");
                    $('.load-modal-content').html(result.html);
                    $('#loadModal').modal('show');                    
                }
            });
        });

        ajaxCall = function($method,$url,$data,callback){
            $.ajax({
                type:$method,
                url:$url,
                data:$data,
                dataType : 'json',
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                success:function(result){
                    callback(result);
                }
            });
        }

        /* 
        ajaxFCall = function($url,$form,callback){
            var formData = new FormData($form);
            $.ajax({
                url : $url,
                type: "post",
                data : formData,
                async : false,
                contentType: false,
                cache: false,
                processData:false,
                dataType : 'json',
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                success:function(result){
                    callback(result);
                }
            });
        }
        */
    });